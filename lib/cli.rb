#!/usr/bin/env ruby

app_dir = File.expand_path('../..', __FILE__)
ENV['BUNDLE_GEMFILE'] = app_dir + '/Gemfile'

require 'bundler'
require "colorize"

require_relative '../lib/solver'

puts "Хей! Это простой решатор уравнений 2-ой степени вида ax^2+bx+c=0"

args = %w(a b c).map.with_index do |arg_name, index|
  begin
    print "Введите аргумент #{arg_name}: "
    input = gets.chomp
    input = Float(input)
    raise if index == 0 && input == 0
    input
  rescue
    puts "Некорректный ввод. Введите заново.".red
    redo
  end
end

xarr = solve_equation(*args)
puts "Корни уравнения: " + xarr.join(', ')
