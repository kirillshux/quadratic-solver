def solve_equation(*args)
  a, b, c = args[0..2].map(&:to_f)
  d = (b**2) - (4 * a * c)

  if d == 0
    x1 = -b / (2*a)
    [x1]
  elsif d > 0
    x1 = (-b + Math.sqrt(d)) / (2 * a)
    x2 = (-b - Math.sqrt(d)) / (2 * a)
    [x1, x2]
  else
    []
  end
end
