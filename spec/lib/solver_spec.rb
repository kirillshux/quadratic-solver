require 'spec_helper'
require_relative '../../src/solver'

describe 'Solver' do
  subject { solve_equation(*args) }

  context 'when we pass args waiting 2 results' do
    let(:args) { [1, -4, 3] }

    it 'should be ok' do
      expect(subject).to eq [3, 1]
    end
  end

  context 'when we pass args waiting 1 result' do
    let(:args) { [3, -6, 3] }

    it 'should be ok' do
      expect(subject).to eq [1]
    end
  end

  context 'when we pass args waiting no result' do
    let(:args) { [4, 5, 3] }

    it 'should be ok' do
      expect(subject).to eq []
    end
  end
end
